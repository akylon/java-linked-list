public class SllNode {
  int data;
  SllNode next;

  public SllNode(int data){
    this.data = data;
    this.next = null;
  }
}
