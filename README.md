# java linked list

## what
Linked list and doubly linked list classes in java.
Currently only append operation supported.

## why
Learning purpose.

For translating to zig.

## how to use
* create new directory
* put all files in new directory
* javac *.java
* java Main

## licence 
MIT

## jdk
openjdk 17.0.3-internal 2022-04-19

OpenJDK Runtime Environment (build 17.0.3-internal+0-adhoc.root.jdk17u-jdk-17.0.3-ga)

OpenJDK 64-Bit Server VM (build 17.0.3-internal+0-adhoc.root.jdk17u-jdk-17.0.3-ga, mixed mode, sharing)
