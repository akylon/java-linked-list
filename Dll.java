/*
 * Dll = doubly linked list
 */


public class Dll {
  DllNode head;
  public Dll(){
    this.head = null;
  }

  public void append(int data){
    DllNode tmp = new DllNode(data);

    if(this.head == null){//if starting
      this.head = tmp;
      this.head.next = null;
      this.head.prev = null;
      return;
    }
    
    else {
      //find last Node
      DllNode tmp2 = this.head;
      while(tmp2.next != null){
        tmp2 = tmp2.next;
      }
      
      //add tmp to end of list
      tmp.prev = tmp2;
      tmp2.next = tmp;
    }
  }

  /*
   * Just print data in from first to last
   */
  public void dllPrint(){
    DllNode tmp = this.head;
    while(tmp != null){
      System.out.println(tmp.data);
      tmp = tmp.next;
    }
  }
}
