public class DllNode {
  int data;
  DllNode prev;
  DllNode next;
  public DllNode(int data){
    this.data = data;
    this.prev = null;
    this.next = null;
  }
}
