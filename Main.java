public class Main {
  public static void main(String[] args){

    System.out.println("~~~ Linked list ~~~");
    Sll sll = new Sll();
    sll.append(1);
    sll.append(56);
    sll.append(89);
    sll.append(1256);
    sll.sllPrint();


    System.out.println("~~~ Doubly linked list ~~~");
    Dll list = new Dll();
    list.append(1213);
    list.append(0);
    list.append(45);
    list.append(23);
    list.dllPrint();

    //System.out.println(list.head.prev);//null
    //System.out.println(list.head.next.next.prev.data); //0
  }
}
