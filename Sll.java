/*
 * Sll = simple linked list 
 */

public class Sll {
  SllNode head;
  public Sll(){
    this.head = null;
  }
  public void append(int newVal){
    SllNode tmp = new SllNode(newVal);

    if(this.head == null){ //this if happens when initializing sll
      this.head = tmp;
    }

    else {//find last node and append tmp
      SllNode tmp2 = this.head;
      while(tmp2.next != null){
        tmp2 = tmp2.next;
      }
      tmp2.next = tmp;
    }
  }

  public void sllPrint(){
    SllNode node = this.head;
    while(node!=null){
      System.out.println(node.data);
      node = node.next;
    }
  }
}
